import styled, { css } from 'styled-components';

export const FlexContainer = styled.div<{ variant?: 'vertical' | 'horizontal' }>(
  ({ variant = 'horizontal' }) => css`
    display: flex;
    height: 100%;
    flex-direction: ${variant === 'vertical' ? 'column' : 'row'};
  `
);

export const LeftSidePanel = styled.div`
  flex: 3;
`;
export const RightSidePanel = styled.div`
  flex: 1;
`;

export const MainSection = styled.div`
  flex: 6;
`;

export const MainSectionTopArea = styled.div`
  flex: 3;
`;
export const MainSectionBottomArea = styled.div`
  flex: 1;
`;
