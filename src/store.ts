import { ITimedPoint } from './types/IRoute';

export enum FetchStatus {
  Idle,
  Loading,
  Success,
  Error,
}

interface State {
  status: FetchStatus;
  takeoffTime: number;
  landingTime: number;
  routePoints: ITimedPoint[] | null;
}

type Actions =
  | { type: FetchStatus.Loading }
  | { type: FetchStatus.Success; takeoffTime: number; landingTime: number; routePoints: ITimedPoint[] };

export const reducer: React.Reducer<State, Actions> = (state: State, action) => {
  switch (action.type) {
    case FetchStatus.Loading:
      return { ...state, status: action.type };
    case FetchStatus.Success:
      return {
        ...state,
        status: action.type,
        takeoffTime: action.takeoffTime,
        landingTime: action.landingTime,
        routePoints: action.routePoints,
      };
    default:
      return state;
  }
};
