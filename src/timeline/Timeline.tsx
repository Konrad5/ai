import React from 'react';
import styled, { css } from 'styled-components';
import { addTimelineBuffer } from './heplers';
import { useTimelineMarks } from './useTimelineMarks';

export const Timeline: React.FC<{ takeOffTime?: number; landingTime?: number; onChange: (e: React.ChangeEvent) => void }> = ({
  takeOffTime = 0,
  landingTime = 0,
  onChange,
}) => {
  const { min: minValueWithBuffer, max: maxValueWithBuffer } = addTimelineBuffer(takeOffTime, landingTime);

  const { landingMarkRef, takeOffMarkRef } = useTimelineMarks(takeOffTime, landingTime);

  return (
    <RangeInputWrapper>
      <VerticalMark ref={takeOffMarkRef}>
        <Title>Take-off</Title>
      </VerticalMark>
      <RangeInput
        min={minValueWithBuffer}
        max={maxValueWithBuffer}
        onChange={onChange}
      />
      <VerticalMark ref={landingMarkRef}>
        <Title>Landing</Title>
      </VerticalMark>
    </RangeInputWrapper>
  );
};

const RangeInputWrapper = styled.div`
  position: relative;
  display: flex;
  height: 100%;
  width: 100%;
`;

const AbsoluteCentering = css`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;

const RangeInput = styled.input.attrs(() => ({
  type: 'range',
}))(() => [
  css`
    -webkit-appearance: none;
    appearance: none;
    height: 0.125rem;
    margin: 0;
    outline: 0;
    width: 100%;
    background: #c2c6d3;
    ${AbsoluteCentering}

    &::-ms-track {
      background: transparent;
      border-color: transparent;
      color: transparent;
    }
    &::-webkit-slider-thumb {
      -webkit-appearance: none;
      ${thumbBaseStyle}
    }
    &::-moz-range-thumb {
      ${thumbBaseStyle}
    }
    &::-ms-thumb {
      ${thumbBaseStyle}
    }
    &:focus::-webkit-slider-thumb {
      border-radius: 50%;
    }
    &:focus::-moz-range-thumb {
      border-radius: 50%;
    }
    &:focus::-ms-thumb {
      border-radius: 50%;
    }
  `,
  css`
    &::-webkit-slider-thumb {
      background: #ffff;
    }
    &::-moz-range-thumb {
      background: #ffff;
    }
    &::ms-thumb {
      background: #ffff;
    }
  `,
]);

const thumbBaseStyle = css`
  appearance: none;
  border-radius: 50%;
  border: 0.125rem solid #406ad3;
  cursor: pointer;
  height: 1rem;
  width: 1rem;
`;

const VerticalMark = styled.div`
  position: absolute;
  top: 0;
  border-left: 4px dotted #898a8d;
  height: 3rem;
  z-index: -1;
  ${AbsoluteCentering};
`;

const Title = styled.div`
  ${AbsoluteCentering}
  font-size: 11px;
  font-weight: 500;
  color: #898a8d;
  width: max-content;
  top: 4rem;
`;
