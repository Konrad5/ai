import { TIMELINE_BUFFER_HOURS } from '../config';

const HoursToSeconds = (h: number) => h * 60 * 60;

export const addTimelineBuffer = (min: number, max: number) => {
  return { min: min - HoursToSeconds(TIMELINE_BUFFER_HOURS), max: max + HoursToSeconds(TIMELINE_BUFFER_HOURS) };
};
