import { useRef, useEffect } from 'react';
import { addTimelineBuffer } from './heplers';

export const useTimelineMarks = (takeOffTime: number, landingTime: number) => {
  const takeOffMarkRef = useRef<HTMLDivElement>(null);
  const landingMarkRef = useRef<HTMLDivElement>(null);

  useEffect(() => {
    if (takeOffTime > 0 && landingTime > 0 && takeOffMarkRef.current && landingMarkRef.current) {
      const { min, max } = addTimelineBuffer(takeOffTime, landingTime);

      const minValuePercentageDistance = setMarkPosition(takeOffTime, min, max);
      // computation includes workaround for getting slider thumb measurments
      takeOffMarkRef.current.style.left = `calc(${minValuePercentageDistance}% + (${
        8 - minValuePercentageDistance * 0.16
      }px))`;

      const maxValuePercentageDistance = setMarkPosition(landingTime, min, max);
      landingMarkRef.current.style.left = `calc(${maxValuePercentageDistance}% + (${
        8 - maxValuePercentageDistance * 0.16
      }px))`;
    }
  });

  return { takeOffMarkRef, landingMarkRef };
};

const setMarkPosition = (value: number, min: number, max: number) => {
  const timelineDistance = max - min;
  const minValueDistance = value - min;
  return (minValueDistance / timelineDistance) * 100;
};
