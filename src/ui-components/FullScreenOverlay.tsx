import styled from 'styled-components';

export const FullScreenOverlay = styled.div<{ variant: 'dark' | 'light' }>`
  position: absolute;
  top: 0;
  right: 0;
  bottom: 0;
  left: 0;
  background: ${props => (props.variant === 'dark' ? '#c2c6d3' : '#ffff')};
  z-index: 1;
`;

export const OverlayItem = styled.div`
  position: absolute;
  left: 50%;
  top: 50%;
  transform: translate(-50%, -50%);
`;
