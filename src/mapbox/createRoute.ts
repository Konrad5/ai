import { ITimedPoint } from "../types/IRoute";
import { mapToMapboxRouteCoordinates } from "./helpers";

export function createRoute(
    mapbox: any,
    data: ITimedPoint[],
    styles = {
      layout: {
        'line-cap': 'round',
      },
      paint: {
        'line-color': '#020202',
        'line-width': 2,
        'line-dasharray': [1, 2],
      },
    }
  ) {
    mapbox.addSource('route', {
      type: 'geojson',
      data: {
        type: 'Feature',
        properties: {},
        geometry: {
          type: 'LineString',
          coordinates: mapToMapboxRouteCoordinates(data),
        },
      },
    });
    mapbox.addLayer({
      id: 'route',
      type: 'line',
      source: 'route',
      ...styles,
    });
  }
  