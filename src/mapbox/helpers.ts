import { ITimedPoint } from '../types/IRoute';

export const mapToMapboxWaypointsData = (points: ITimedPoint[]) => {
  return {
    type: 'FeatureCollection' as any,
    features: points.map(({ lng, lat }) => ({
      type: 'Feature',
      geometry: { type: 'Point', coordinates: [lng, lat] },
    })),
  };
};

export const mapToMapboxRouteCoordinates = (points: ITimedPoint[]) => {
  return points.map(({ lng, lat }) => [lng, lat]);
};
