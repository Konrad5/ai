import { ITimedPoint } from '../types/IRoute';
import { changePlaneCoordinates, planeId } from './createPlane';
import { inRange } from 'lodash';

export function movePlane({
  mapbox,
  routePoints,
  timestamp,
}: {
  mapbox: any;
  routePoints: ITimedPoint[];
  timestamp: number;
}) {
  const rangeCandidate = getTimestampRange(routePoints, timestamp);
  const visibility = mapbox.getLayoutProperty(planeId, 'visibility');
  if (isValidRange(rangeCandidate)) {
    const factor = countValueDistanceFactor(timestamp, rangeCandidate[0].timestamp, rangeCandidate[1].timestamp);
    changePlaneCoordinates(mapbox, { ...countPlanePosition(factor, rangeCandidate), timestamp });
    visibility === 'none' && mapbox.setLayoutProperty('plane', 'visibility', 'visible');
  } else {
    mapbox.setLayoutProperty(planeId, 'visibility', 'none');
  }
}

function getTimestampRange(items: ITimedPoint[], value: number): ITimedPoint | ITimedPoint[] {
  let isInRange = false;
  return items.reduce(function (prev: any, curr: ITimedPoint) {
    if (isInRange) {
      return prev;
    } else {
      isInRange = inRange(value, prev.timestamp, curr.timestamp);
      return isInRange ? [prev, curr] : curr;
    }
  });
}

function countValueDistanceFactor(value: number, min: number, max: number) {
  const distance = max - min;
  const valueDistance = value - min;
  return valueDistance / distance;
}

function countPlanePosition(factor: number, [start, end]: ITimedPoint[]) {
  // linear interpolation formula: p = (1-t) x p1 + t x p2
  return { lng: (1 - factor) * start.lng + factor * end.lng, lat: (1 - factor) * start.lat + factor * end.lat };
}

function isValidRange(array: ITimedPoint[] | ITimedPoint): array is ITimedPoint[] {
  return Array.isArray(array) && !!array[0].timestamp;
}
