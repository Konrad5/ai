import { ITimedPoint } from '../types/IRoute';
import { mapToMapboxWaypointsData } from './helpers';

export function createWaypoints(
  mapbox: any,
  data: ITimedPoint[],
  styles = {
    layout: {
      'icon-image': `waypoint`,
      'icon-allow-overlap': true,
      'icon-size': 0.3,
    },
    paint: {},
  }
) {
  mapbox.loadImage(process.env.PUBLIC_URL + './waypoint@3x.png', (error: any, image: any) => {
    if (error) throw error;
    mapbox.addImage('waypoint', image);

    mapbox.addLayer({
      id: 'waypoints',
      type: 'symbol',
      source: {
        type: 'geojson',
        data: mapToMapboxWaypointsData(data),
      },
      ...styles,
    });
  });
}
