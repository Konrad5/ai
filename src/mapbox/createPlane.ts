import { ITimedPoint } from '../types/IRoute';

export const planeId = 'plane';

const defaultSource = {
  type: 'Feature',
  properties: {},
  geometry: {
    type: 'Point',
    coordinates: [0, 0],
  },
};

export function createPlane(
  mapbox: any,
  { lng, lat }: ITimedPoint,
  styles = {
    layout: {
      'icon-image': 'airfield-15',
      'icon-allow-overlap': true,
      'visibility': 'none'
    },
    paint: {},
  }
) {
  mapbox.addSource(planeId, {
    type: 'geojson',
    data: {
      ...defaultSource,
      geometry: {
        ...defaultSource.geometry,
        coordinates: [lng, lat],
      },
    },
  });
  mapbox.addLayer({
    id: planeId,
    type: 'symbol',
    source: planeId,
    ...styles,
  });
}

export function changePlaneCoordinates(mapbox: any, { lng, lat }: ITimedPoint) {
  mapbox.getSource(planeId).setData({
    ...defaultSource,
    geometry: {
      ...defaultSource.geometry,
      coordinates: [lng, lat],
    },
  });
}
