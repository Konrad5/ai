import React, { useRef, useEffect, useReducer, useCallback } from 'react';
import mapboxgl from 'mapbox-gl';
import { fakeApi } from './utils/fakeApi';
import { createRoute, createWaypoints, movePlane } from './mapbox';
import { PlaneSpinner, FullScreenOverlay, OverlayItem } from './ui-components';
import { Timeline } from './timeline/Timeline';
import { createPlane } from './mapbox/createPlane';
import {
  FlexContainer,
  LeftSidePanel,
  MainSection,
  MainSectionBottomArea,
  MainSectionTopArea,
  RightSidePanel,
} from './layout';
import { FetchStatus, reducer } from './store';

mapboxgl.accessToken = 'pk.eyJ1Ijoia29jaSIsImEiOiJja3NsaWVuaHowaWo0Mm9wZnhkZzlldGNnIn0.gig8mgCHCNoOMLg3Daqgfg';

const initialState = {
  takeoffTime: 0,
  landingTime: 0,
  status: FetchStatus.Idle,
  routePoints: null,
};

function App() {
  const mapContainer = useRef<any>(null);
  const map = useRef<any>();
  const [{ status, takeoffTime, landingTime, routePoints }, dispatch] = useReducer(reducer, initialState);

  const handleOnChange = useCallback(
    e => {
      if (routePoints && map.current) {
        movePlane({ mapbox: map.current, routePoints, timestamp: e.target.value });
      }
    },
    [routePoints]
  );

  useEffect(() => {
    const setRoute = async () => {
      try {
        dispatch({ type: FetchStatus.Loading });
        const { route_points } = await fakeApi.fetchRoute();
        let mapbox = new mapboxgl.Map({
          container: mapContainer.current,
          style: 'mapbox://styles/mapbox/streets-v11',
          center: [route_points[0].lng, route_points[0].lat],
          zoom: 8,
        });
        map.current = mapbox;
        mapbox.on('load', () => {
          createWaypoints(mapbox, route_points);
          createRoute(mapbox, route_points);
          createPlane(mapbox, route_points[0]);
        });
        dispatch({
          type: FetchStatus.Success,
          takeoffTime: route_points[0].timestamp,
          landingTime: route_points[route_points.length - 1].timestamp,
          routePoints: route_points,
        });
      } catch (error) {
        throw error;
      }
    };
    setRoute();
  }, []);
  return (
    <FlexContainer>
      {(status === FetchStatus.Loading || status === FetchStatus.Idle) && (
        <FullScreenOverlay variant="dark">
          <OverlayItem>
            <PlaneSpinner color={'white'} />
          </OverlayItem>
        </FullScreenOverlay>
      )}
      <>
        <LeftSidePanel />
        <MainSection>
          <FlexContainer variant="vertical">
            <MainSectionTopArea>
              <div ref={mapContainer} />
            </MainSectionTopArea>
            <MainSectionBottomArea>
              <Timeline takeOffTime={takeoffTime} landingTime={landingTime} onChange={handleOnChange} />
            </MainSectionBottomArea>
          </FlexContainer>
        </MainSection>
        <RightSidePanel />
      </>
    </FlexContainer>
  );
}

export default App;

/* TODO: render plane as top layer, adapt mapbox +'any' types, check problem with export * CRA config, time on markers */
/* TODO: TESTS TESTS TESTS */
